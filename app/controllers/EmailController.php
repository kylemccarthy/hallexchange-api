<?php

class EmailController extends BaseController {

    public function postContactOwner($ownerId, $id)
    {
        $owner  = User::find($ownerId);
        $user   = Auth::user();

        $data = array(
            'content'   => Input::get('message'),
            'first'     => $user->getFirstName(),
            'email'     => $user->getEmail(),
            'id'        => $id,
        );

        // send the email
        Mail::send('emails.contact-owner', $data, function($message)
            use ($user, $owner)
        {
            $message->from("no-reply@hallexchange.com", "HallExchange");
            $message->to($owner->getEmail())->subject("Someone is interested in your ad.");
        });

        return Redirect::to('/ads/view/' . $id)
            ->with('contactSuccess',
                "We have sent your email to the owner to let them know that you
                are interested in their ad.");
    }

}