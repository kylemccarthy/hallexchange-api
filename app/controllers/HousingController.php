<?php

class HousingController extends BaseAdController
{
    public $rules = array(
        'name'     => 'required',
        'content'   => 'required',
        'price'     => 'required',
    );
    public $relation = 'housing';
    public $rootId = 4;
}