<?php

class MessageController extends BaseController {

    /**
     * Get all of a users conversations
     *
     * @return mixed
     */
    public function index()
    {
        $convo = Conversation::where('owner', '=', Auth::user()->id)
            ->orWhere('buyer', '=', Auth::user()->id)
            ->get();
        return Response::json($convo->toArray(), 200);
    }

    /**
     * Create a new conversation for an item
     *
     * @param $ad
     * @return mixed
     */
    public function newConversation($ad)
    {
        $ad = Ad::find($ad);
        if ($ad) {
            $from = Auth::user()->id;
            $to = $ad->user->id;
            $message = Input::get('content');
            // set conversation details
            $convo = array(
                'owner' => $to,
                'buyer' => $from,
                'ad_id' => $ad->id,
            );
            $convo = Conversation::create($convo);
            // set the message information
            $message = array(
                'user_id'  => $from,
                'content' => $message,
                'conversation_id' => $convo->id,
            );
            Message::create($message);
            return Response::json('created conversation', 200);
        }
        return Response::json('invalid ad id', 400);
    }

    /**
     * Get a conversation between two users
     *
     * @param $id
     * @return mixed
     */
    public function getConversation($id)
    {
        $convo = Conversation::with('messages')->find($id);
        if ($convo) {
            // make sure the current user is apart of the conversation
            if ($convo->buyer != Auth::user()->id && $convo->seller != Auth::user()->id) {
                return Response::json('not apart of conversation', 403);
            }
            return Response::json($convo->toArray(), 200);
        }
        return Response::json('invalid conversation id', 400);
    }

    /**
     * Reply to a conversation
     *
     * @param $id
     * @return mixed
     */
    public function reply($id)
    {
        $convo = Convo::find($id);
        $input = Input::all();
        $input['user_id'] = Auth::user()->id;
        $input['conversation_id'] = $convo->id;
        if ($convo) {
            // make sure the current user is apart of the conversation
            if ($convo->buyer != Auth::user()->id && $convo->seller != Auth::user()->id) {
                return Response::json('not apart of conversation', 403);
            }
            $message = Message::create($input);
            $convo->messages()->save($message);
            return Response::json('replied', 200);
        }
        return Response::json('invalid conversation id', 400);
    }
}