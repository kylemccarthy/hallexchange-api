<?php

class FurnitureController extends BaseAdController
{
    public $rules = array(
        'name'     => 'required',
        'content'   => 'required',
        'price'     => 'required',
    );
    public $relation = 'furniture';
    public $rootId = 3;
}