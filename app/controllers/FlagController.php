<?php

class FlagController extends BaseController {

    public function getAd($id)
    {
        // make sure the user is an admin
        User::requireAdmin();

        $ad = Ad::find($id);
        if ($ad) {
            $user = User::find($ad->getUserId());
            return View::make('flag.ad')
                ->with('ad', $ad)
                ->with('user', $user);
        } else {
            Return App::abort('404');
        }
    }

    /**
     * Handle the post request for flagging an ad
     * @param $id
     */
    public function postAd($id)
    {
        // make sure the user is an admin
        User::requireAdmin();

        $rules = array(
            'ad-name'       => 'required',
            'first-name'    => 'required',
            'email'         => 'required',
            'reason'        => 'required',
            'content'       => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('/flag/ad/' . $id)
                ->withErrors($validator)->withInput();
        }

        $data = array(
            'first'         => Input::get('first-name'),
            'last'          => Input::get('last-name'),
            'email'         => Input::get('email'),
            'reason'        => Input::get('reason'),
            'content'       => Input::get('content'),
        );

        Mail::send('emails.flag.ad', $data, function($message) use ($data)
        {
            $message->to($data['email'], $data['first'] . ' ' . $data['last'])
                ->subject('[HallExchange] Ad Removed!');
        });

        Ad::remove($id);

        return Redirect::to('/exchange')->with('success', 'The ad has been removed and the user has
        been notified.');
    }
}