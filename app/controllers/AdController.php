<?php

class AdController extends BaseController
{
    public function index()
    {
        $models = Ad::where('university_id', '=', Auth::user()->university_id)
            ->with(array('images' => function($q) {
                    $q->groupBy('ad_id');
                }))
            ->orderBy('created_at', 'desc')
            ->remember(.5)->paginate(100);
        return Response::json($models, 200);
    }

    public function universityGroup()
    {
        $university = University::find(Auth::user()->university_id);
        $group = University::where('university_group_id', '=', $university->university_group_id)->lists('id');
        $models = Ad::whereIn('university_id', $group)
            ->with(array('images' => function($q) {
                    $q->groupBy('ad_id');
                }))
            ->orderBy('created_at', 'desc')
            ->remember(.5)->paginate(100);
        return Response::json($models, 200);
    }

    public function show($id)
    {
        $ad = Ad::findOrFail($id);
        $category = Category::findOrFail($ad->category_id)->table_name;
        $model = $ad->$category;
        return Response::json(array(
                $ad->toArray(),
                $model->toArray()
            ), 200);
    }

    public static function destroy($id)
    {
        $ad = Ad::findOrFail($id);
        $category = Category::findOrFail($ad->category_id)->table_name;
        $model = $ad->$category;
        $ad->images()->delete();
        $ad->delete();
        $model->delete();
        return Response::json(200);
    }
}