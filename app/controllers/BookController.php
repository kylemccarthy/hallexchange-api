<?php

class BookController extends BaseAdController
{
    public $rules = array(
        'name'     => 'required',
        'content'   => 'required',
        'price'     => 'required',
    );

    public $relation = 'book';
    public $rootId = 1;
}