<?php

class ImageController extends BaseController {

    public function store($adId)
    {
        $input = Input::all();

        $rules = array(
            'file' => 'image|max:3000',
        );

        $validation = Validator::make($input, $rules);

        if ($validation->fails())
        {
            return Response::make('error: invalid file type or size', 400);
        }

        $filename = ImageService::upload(Input::file('file'));

        $image = new Image();
        $image->setAdId($adId);
        $image->setUserId($userId);
        $image->setImageUrl($filename);
        $image->save();

        return Response::json('success', 200);
    }
}