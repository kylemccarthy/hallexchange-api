<?php

class PromotionController extends BaseAdController
{
    public $rules = array(
        'name'      => 'required',
        'content'   => 'required',
    );
    public $relation = 'promotion';
    public $rootId = 8;
}