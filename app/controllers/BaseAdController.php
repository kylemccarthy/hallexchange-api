<?php

abstract class BaseAdController extends BaseController
{
    public $rules;
    public $relation;
    public $rootId;
    private $imageRules = array(
        'image'    => 'image|max:20000',
    );

    public function index()
    {
        $models = Ad::where('university_id', '=', 1)
            ->with(array($this->relation, 'images' => function($q) {
                    $q->groupBy('ad_id');
                }))->where('category_id', '=', $this->rootId)
            ->orderBy('created_at', 'desc')
            ->remember(.5)->paginate(100);
        return Response::json($models, 200);
    }

    public function universityGroup()
    {
        $university = University::find(Auth::user()->university_id);
        $group = University::where('university_group_id', '=', $university->university_group_id)->lists('id');
        $models = Ad::whereIn('university_id', $group)
            ->with(array($this->relation, 'images' => function($q) {
                $q->groupBy('ad_id');
            }))->where('category_id', '=', $this->rootId)
            ->orderBy('created_at', 'desc')
            ->remember(.5)->paginate(100);
        return Response::json($models, 200);
    }

    public function store()
    {
        $validator = Validator::make(Input::all(), $this->rules);
        $fileValidator = true;
        if (Input::has('image'))
            $fileValidator = Validator::make(Input::file('image'), $this->imageRules);
        if ($validator->passes() && $fileValidator) {
            $relation = $this->relation;
            $class = ucfirst($relation);
            $model = $class::create(Input::all());
            $model->category_id = $this->rootId;
            $model->user_id = Auth::user()->id;
            $model->university_id = Auth::user()->university_id;
            $model->save();
            if (Input::has('image'))
                Image::upload(array($model, Input::file('image')));
            $model = array(
                $model->toArray(),
                $model->$relation->toArray(),
                $model->images->toArray(),
            );
            return Response::json($model, 200);
        } else {
            $messages = $validator->messages();
            if (Input::has('image')) {
                $fileMessages = $fileValidator->messages();
                return Response::json(
                    ['messages' => [$messages->all(), $fileMessages->all()]],
                    400
                );
            }
            return Response::json(
                ['messages' => [$messages->all()]],
                400
            );
        }
    }

    public function update($id)
    {
        $ad = Ad::findOrFail($id);
        $relation = $this->relation;
        $model = $ad->$relation;
        $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->passes()) {
            $ad->fill(Input::all());
            $model->fill(Input::all());
            $ad->push();
            return Response::json($ad, 200);
        } else {
            return Response::json(
                ['messages' => $validator->messages()],
                400
            );
        }
    }

}