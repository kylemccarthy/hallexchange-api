<?php

class HomeController extends BaseController {

	public function recentAds()
    {
        $ads = Ad::where('featured', '=', true)
            ->with(array('images' => function($q) {
                $q->groupBy('ad_id');
            }))->orderBy('created_at', 'DESC')->remember(5)->take(3)->get();
        return Response::json($ads, 200);
    }

    public function universities()
    {
        $universities = University::with('group', 'domains')->remember(5)->get();
        return Response::json($universities, 200);
    }

}