<?php

class ElectronicController extends BaseAdController
{
    public $rules = array(
        'name'     => 'required',
        'content'   => 'required',
        'price'     => 'required',
    );
    public $relation = 'electronic';
    public $rootId = 2;
}