<?php

class TicketController extends BaseAdController
{
    public $rules = array(
        'name'     => 'required',
        'content'   => 'required',
        'price'     => 'required',
    );
    public $relation = 'ticket';
    public $rootId = 6;
}