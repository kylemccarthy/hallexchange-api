<?php

class RideController extends BaseAdController
{
    public $rules = array(
        'name'          => 'required',
        'content'       => 'required',
        'type'          => 'required',
        'coming_from'   => 'required',
        'going_to'      => 'required',
    );
    public $relation = 'ride';
    public $rootId = 5;
}