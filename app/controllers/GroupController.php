<?php

class GroupController extends BaseController {

    /**
     * Show all of the groups of a school
     */
    public function getIndex()
    {
        $greeks = Group::where('university_id', '=', Auth::user()->getUniversityId())
            ->where('category', '=', 1)
            ->where('privacy', '!=', 0)
            ->where('active', '=', 1)
            ->orderBy('name', 'asc')
            ->remember(2)->get();
        $oncampus = Group::where('university_id', '=', Auth::user()->getUniversityId())
            ->where('category', '=', 2)
            ->where('privacy', '!=', 0)
            ->where('active', '=', 1)
            ->orderBy('name', 'asc')
            ->remember(2)->get();
        $offcampus = Group::where('university_id', '=', Auth::user()->getUniversityId())
            ->where('category', '=', 3)
            ->where('privacy', '!=', 0)
            ->where('active', '=', 1)
            ->orderBy('name', 'asc')
            ->remember(2)->get();
        $uncategorized = Group::where('university_id', '=', Auth::user()->getUniversityId())
            ->where('category', '=', 0)
            ->where('privacy', '!=', 0)
            ->orderBy('name', 'asc')
            ->remember(2)->get();

        return View::make('groups.index')
            ->with('greeks', $greeks)
            ->with('oncampus', $oncampus)
            ->with('offcampus', $offcampus)
            ->with('uncategorized', $uncategorized);
    }

    /**
     * Method for displaying the form for creating a new group
     */
    public function getNew()
    {
        $groupOptions = DB::table('group_categories')
            ->remember(120)
            ->orderBy('id', 'asc')
            ->lists('name','id');
        return View::make('groups.new')->with('groupCategories', $groupOptions);
    }

    /**
     * Handle the post request for the group create page.  Validate it for name
     * and description then create and redirect.
     */
    public function postNew()
    {
        $validator = Group::groupValidate(Input::all());

        if ($validator->passes()) {

            if (Input::get('category') == 0) {
                // create the new group
                $group = new Group();
                $group->setGroupName(Input::get('name'));
                $group->setGroupDescription(Input::get('description'));
                $group->setUniversityId(Auth::user()->getUniversityId());
                $group->setCategory(Input::get('category'));
                $group->activate();
                $group->setPrivacy(0);
                $group->save();

                // set the user that created it as the admin
                $admin = new GroupItem();
                $admin->setUserId(Auth::user()->getUserId());
                $admin->setGroupId($group->getGroupId());
                $admin->setLevel(10);
                $admin->save();

                return Redirect::to('/groups/admin/' . $group->getGroupId())
                    ->with('success', 'You have created a new group.  You can now
                 add and manage users');
            } else {
                // create the new group
                $group = new Group();
                $group->setGroupName(Input::get('name'));
                $group->setGroupDescription(Input::get('description'));
                $group->setUniversityId(Auth::user()->getUniversityId());
                $group->setCategory(Input::get('category'));
                $group->deactivate();
                $group->setPrivacy(1);
                $group->save();

                // set the user that created it as the admin
                $admin = new GroupItem();
                $admin->setUserId(Auth::user()->getUserId());
                $admin->setGroupId($group->getGroupId());
                $admin->setLevel(5);
                $admin->save();

                return Redirect::to('/groups/new')
                    ->with('success', 'You have created a new group.  This type
                    of group needs to be approved by our admins.');
            }

        } else {
            return Redirect::to('/groups/new')->withErrors($validator)
                ->withInput();
        }
    }

    /**
     * Handle the requests for viewing all of the ads in a particular group.  This will
     * also allow for users to sort the ads by their categories in a group.
     */
    public function getView($groupId, $table = null)
    {
        $name = null;

        // check to make sure the groups exists and that the user is a member in the group
        $member = GroupItem::where('user_id', '=', Auth::user()->getUserId())
            ->where('group_id', '=', $groupId)->first();

        $group = Group::find($groupId);

        if (!$member) {
            App::abort('403');
        }

        if ($table == null) {
            $ads = Ad::where('group_id', '=', $groupId)
                ->orderBy('created_at', 'desc')
                ->with('images')
                ->paginate(15);
            return View::make('groups.view')
                ->with('ads', $ads)
                ->with('groupId', $groupId)
                ->with('groupName', $group->getGroupName())
                ->with('memberId', $member->getId())
                ->with('status', $group->getStatus());
        } else {
            $categoryId     = null;
            $categoryTable  = null;

            if ($table == "books") {
                $categoryId = 1;
                $categoryTable = "book";

            } else if ($table == "electronics") {
                $categoryId = 2;
                $categoryTable = "electronic";

            } else if ($table == "furniture") {
                $categoryId = 3;
                $categoryTable = "furniture";

            } else if ($table == "housing") {
                $categoryId = 4;
                $categoryTable = "housing";

            } else if ($table == "rides") {
                $categoryId = 5;
                $categoryTable = "rides";

            } else if ($table == "tickets") {
                $categoryId = 6;
                $categoryTable = "ticket";

            } else if ($table == "tutoring") {
                $categoryId = 9;
                $categoryTable = "tutor";

            } else if ($table == "promotions") {
                $categoryId = 8;
                $categoryTable = "promotion";

            } else if ($table == "misc") {
                $categoryId = 7;
                $categoryTable = "promotion";

            } else {
                return App::abort(404);
            }

            $ads = Ad::where('group_id', '=', $groupId)
                ->where('category_id', '=', $categoryId)
                ->orderBy('created_at', 'desc')
                ->with('images', $categoryTable)
                ->paginate(15);

            return View::make('groups.view')
                ->with('ads', $ads)
                ->with('groupId', $groupId)
                ->with('groupName', $group->getGroupName())
                ->with('memberId', $member->getId())
                ->with('status', $group->getStatus());
        }
    }

    /**
     * Show the admin panel for the group.  Make sure that the user is an admin.
     */
    public function getAdmin($groupId)
    {
        // check to make sure the groups exists and that the user is a member in
        // the group
        $member = GroupItem::where('user_id', '=', Auth::user()->getUserId())
            ->where('group_id', '=', $groupId)->first();

        if (!$member || $member->getLevel() < 10) {
            App::abort('403');
        }

        $group = Group::find($groupId);

        $requests = GroupRequest::where('group_id', '=', $groupId)
            ->with('user')->get();
        $groupMembers = GroupItem::where('group_id', '=', $groupId)->get();

        return View::make('groups.admin')
            ->with('members', $groupMembers)
            ->with('groupId', $groupId)
            ->with('privacy', $group->getPrivacy())
            ->with('requests', $requests);
    }

    /**
     * Handle deleting users from a group.  The user must be an admin of the group
     */
    public function getRemoveUser($groupId, $userId)
    {
        // check to make sure that the group exists and the user is an admin
        $currentUser = GroupItem::where('user_id', '=', Auth::user()->getUserId())
            ->where('group_id', '=', $groupId)->first();

        $member = GroupItem::find($userId);

        if(!$member || !$currentUser) {
            App::abort('403');
        }

        // check to see if the admin wants to delete the group or remove the user
        if ($currentUser->getLevel() == 10) {

            $user = GroupItem::find($userId);
            $admins = GroupItem::where('group_id', '=', $groupId)->where('level', '=', 10);

            // make sure the user isn't the only admin in a group
            if ($admins->count() <= 1 && $user->getLevel() == 10) {
                return Redirect::to('/groups/admin/' . $groupId)
                    ->with('error', 'There must be another admin in the group');
            }

            $user->delete();

            return Redirect::to('/groups/admin/' . $groupId)
                ->with('success', 'The user was removed from the group.');

        } else if ($member->getUserId() == Auth::user()->getUserId()) {
            $user = GroupItem::find($userId);
            $user->delete();
            return Redirect::to('/exchange')->with('success', 'You have removed yourself from the group.');
        }

        App::abort('403');

    }

    /**
     * Handle the request to remove a group.  Delete the group and all of its members.
     * @param $groupId
     */
    public function getRemoveGroup($groupId)
    {
        // check to make sure that the group exists and the user is an admin
        $member = GroupItem::where('user_id', '=', Auth::user()->getUserId())
            ->where('group_id', '=', $groupId)->first();

        if (!$member || $member->getLevel() < 10) {
            App::abort('403');
        }

        // remove group
        $group = Group::find($groupId);
        $group->delete();

        // remove members
        $members = GroupItem::where('group_id', '=', $groupId)->get();
        foreach ($members as $member) {
            $member->delete();
        }

        return Redirect::to('/groups');
    }

    /**
     * Handle promoting users in a group to admin
     */
    public function getPromote($groupId, $userId)
    {
        // check to make sure that the group exists and the user is an admin
        $member = GroupItem::where('user_id', '=', Auth::user()->getUserId())
            ->where('group_id', '=', $groupId)->first();

        if (!$member || $member->getLevel() < 10) {
            App::abort('403');
        }

        $user = GroupItem::find($userId);

        // if the user exits promote them
        if ($user) {

            $user->setLevel(10);
            $user->save();

            return Redirect::to('/groups/admin/' . $groupId)
                ->with('success', 'The user was promoted');
        } else {
            return Redirect::to('/groups/admin/' . $groupId)
                ->with('error', 'The user could not be found');
        }
    }

    /**
     * Handle promoting users in a group to admin
     */
    public function getDemote($groupId, $userId)
    {
        // check to make sure that the group exists and the user is an admin
        $member = GroupItem::where('user_id', '=', Auth::user()->getUserId())
            ->where('group_id', '=', $groupId)->first();

        if (!$member || $member->getLevel() < 10) {
            App::abort('403');
        }

        // make sure the user isn't the only admin in a group
        if (GroupItem::where('group_id', '=', $groupId)->where('level', '=', 10)->count() <= 1) {
            return Redirect::to('/groups/admin/' . $groupId)
                ->with('error', 'There must be another admin in the group');
        }

        $user = GroupItem::find($userId);

        // if the user exits promote them
        if ($user) {

            $user->setLevel(0);
            $user->save();

            return Redirect::to('/groups/admin/' . $groupId)
                ->with('success', 'The user was promoted');

        } else {
            return Redirect::to('/groups/admin/' . $groupId)
                ->with('error', 'The user could not be found');
        }
    }

    /**
     * Handle the request for an admin adding a new member to their group.
     *
     * @param $groupId
     * @return mixed
     */
    public function postAddMember($groupId)
    {
        // make sure the person is an admin
        $member = GroupItem::where('user_id', '=', Auth::user()->getUserId())
            ->where('group_id', '=', $groupId)->first();

        if (!$member || $member->getLevel() < 10) {
            App::abort('403');
        }

        // try to create a new user for the group
        if (Input::has('email')) {

            // pick the user from the collection and update them
            $user = User::where('email', '=', Input::get('email'))->first();

            if (!$user) {
                return Redirect::to('/groups/admin/' . $groupId)
                    ->with('error', 'The user could not be found');
            }

            // make sure the user isn't already in the group
            $member = GroupItem::where('user_id', '=', $user->getUserId())
                ->where('group_id', '=', $groupId)->first();

            if ($member) {
                return Redirect::to('/groups/admin/' . $groupId)
                    ->with('error', 'The user is already a member');
            }

            // if the email was valid create the new user
            if ($user) {
                $newGroupItem = new GroupItem();
                $newGroupItem->setGroupId($groupId);
                $newGroupItem->setUserId($user->getUserId());
                $newGroupItem->setLevel(0);
                $newGroupItem->save();

                return Redirect::to('/groups/admin/' . $groupId)
                    ->with('success', 'The user has been added to the group');
            }
        }

        return Redirect::to('/groups/admin/' . $groupId)
            ->with('error', 'The user could not be found');

    }

    /**
     * Handle the request for a change of privacy settings for a group.
     *
     * @param $groupId
     * @param $newPrivacy
     * @return mixed
     */
    public function getPrivacy($groupId, $newPrivacy)
    {
        // check to make sure that the group exists and the user is an admin
        $member = GroupItem::where('user_id', '=', Auth::user()->getUserId())
            ->where('group_id', '=', $groupId)->first();

        if ($member) {
            $group = Group::find($groupId);
            $group->setPrivacy($newPrivacy);
            $group->save();
            return Redirect::to("/groups/admin/" . $groupId);
        } else {
            App::abort('403');
        }
    }


    /**
     * Process the action for when an admin of the site denies a user's request for a group and
     * send them an email with a reason.
     *
     * @param $id
     * @return mixed
     */
    public function postAdminDeny($id)
    {
        if (Auth::user()->getLevel() < 10) {
            App::abort('403');
        }

        $group = Group::find($id);
        $groupCreator = GroupItem::where('group_id', '=', $id)->first();
        $user = User::where('id', '=', $groupCreator->getUserId())->first();

        $data = array(
            'firstName' => $user->getFirstName(),
            'groupName' => $group->getGroupName(),
            'content'   => Input::get('content'),
        );

        // send the activation email
        Mail::send('emails.deny-group', $data, function($message) use ($user)
        {
            $message->from("no-reply@hallexchange.com", "HallExchange");
            $message->to($user->getEmail())->subject("[HallExchange] Group Request Denied");
        });

        $group->delete();
        $groupCreator->delete();

        return Redirect::to('/admin');
    }

    /**
     * Process the action for an admin of the site approving a group and then add
     * Connor as the admin for the group.
     *
     * @param $id
     * @return mixed
     */
    public function getAdminApprove($id)
    {
        if (Auth::user()->getLevel() < 10) {
            App::abort('403');
        }

        $group = Group::find($id);
        $group->activate();
        $group->save();

        $creator = GroupItem::where('group_id', '=', $id)->first();;

        $user = User::where('email', '=', 'cdhty8@mail.missouri.edu')->first();
        $admin = new GroupItem();
        $admin->setLevel(10);
        $admin->setUserId($user->getUserId());
        $admin->setGroupId($group->getGroupId());
        $admin->save();

        if ($creator->getUserId() == $user->getUserId()) {
            $creator->delete();
        }

        return Redirect::to('/admin');
    }

    /**
     * Get the user request, create a new member for the group, then delete
     * the older user request and redirect back to admin page.
     *
     * @param $id
     * @return mixed
     */
    public function getApproveUser($id)
    {
        $userRequest = GroupRequest::find($id)->first();
        $newMember = new GroupItem();
        $newMember->setGroupId($userRequest->getGroupId());
        $newMember->setUserId($userRequest->getUserId());
        $newMember->setLevel(0);
        $newMember->save();
        $userRequest->delete();

        return Redirect::to('/groups/admin/' . $newMember->getGroupId());
    }

    /**
     * Handle the action for an admin denying a user access to a group.
     *
     * @param $id
     * @return mixed
     */
    public function getDenyUser($id)
    {
        $userRequest = GroupRequest::find($id)->first();
        $groupId = $userRequest->getGroupId();
        $userRequest->delete();
        return Redirect::to('/groups/admin/' . $groupId);
    }

    /**
     * Create a new request for a user when they want to join a request only
     * group then redirect to the group page with a message.
     *
     * @param $id
     * @return mixed
     */
    public function getRequest($id)
    {
        $group = Group::find($id);
        if (!$group) {
            return Redirect::to("/groups")
                ->with('error', "We can't find that group in our database.  It may have recently been deleted.");
        }
        $userCheck = GroupItem::where('user_id', '=', Auth::user()->getUserId())
            ->where('group_id', '=', $id)->first();
        if (!$userCheck) {
            $request = new GroupRequest();
            $request->setGroupId($id);
            $request->setUserId(Auth::user()->getUserId());
            $request->save();
            return Redirect::to("/groups")->with('success', 'We have processed your request
            to join the group.  The admin of the group must approve you.');
        } else {
            return Redirect::to("/groups")->with('warning', 'You are already a member of this group.');
        }
    }

    public function getJoin($id)
    {
        $group = Group::find($id);
        if (!$group) {
            return Redirect::to("/groups")
                ->with('error', "We can't find that group in our database.  It may have recently been deleted.");
        }
        $userCheck = GroupItem::where('user_id', '=', Auth::user()->getUserId())
            ->where('group_id', '=', $id)->first();
        if (!$userCheck) {
            $join = new GroupItem();
            $join->setGroupId($id);
            $join->setUserId(Auth::user()->getUserId());
            $join->setLevel(0);
            $join->save();
            return Redirect::to("/groups")->with('success', 'You have been added to the group.');
        } else {
            return Redirect::to("/groups")->with('warning', 'You are already a member of this group.');
        }
    }
}