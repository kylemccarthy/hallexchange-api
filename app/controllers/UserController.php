<?php

class UserController extends BaseController {

    public function posts()
    {
        $userId = Auth::id();
        $posts = Ad::where('user_id', '=', $userId)->get();
        return Response::json($posts, 200);
    }

    public function index()
    {
        return Response::json(Auth::user(), 200);
    }

    public function update()
    {
        $user = Auth::user();
        $rules = array(
            'first_name'    => 'required|min:2',
            'last_name'     => 'required',
            'password'      => 'min:8',
            'password_confirmation' => 'same:password',
            'phone'         => 'numeric'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Response::json(
                ['messages' => $validator->messages()], 400);
        } else {
            $user->fill(Input::all());
            $user->save();
            return Response::json('updated', 200);
        }
    }

    public function login()
    {
        $headers = $_SERVER;
        return Apiauth::authenticate(Input::all(), $headers['HTTP_CLIENT_ID']);
    }

    public function store()
    {
        Validator::extend('schoolEmail', function($attribute, $value, $parameters)
        {
            $tmpDomain = explode('@', $value);
            $domain = array_pop($tmpDomain);
            $uni = UniversityDomain::where('domain', '=', $domain)->first();
            if ($uni) {
                return true;
            } else {
                return false;
            }
        });

        $rules = array(
            'first_name'    => 'required',
            'email'         => 'required|email|schoolEmail|unique:users',
            'password'      => 'required|min:8',
            'password-confirmation' => 'required|same:password',
            'tos'           => 'accepted',
        );

        $attributes = array(
            'tos'           => 'terms of service',
            'first-name'    => 'first name',
            'password-confirmation' => 'password confirmation',
        );

        $messages = array(
            'email.school_email' => 'You must register with your school email.',
        );

        $validator = Validator::make(Input::all(), $rules, $messages);
        $validator->setAttributeNames($attributes);

        if ($validator->passes()) {

            // create the user
            $user = new User();
            $user->fill(Input::all());
            $user->email = Input::get('email');

            // find what uni they belong to based on the email they signed up with
            $tmpDomain = explode('@', $user->email);
            $domain = array_pop($tmpDomain);
            $uni = UniversityDomain::where('domain', '=', $domain)->first();
            $user->university_id = $uni->id;

            // hash the password and set the activation code
            $user->password = Hash::make(Input::get('password'));
            $user->auth = Str::random(40);

            // save
            $user->save();

            // send them the activation email
            $data = array(
                'id' => $user->id,
                'auth' => $user->auth,
            );

            Mail::send('register.activate', $data, function($message) use ($user)
            {
                $message->from("no-reply@hallexchange.com", "HallExchange");
                $message->to($user->email)->subject("[HallExchange] Activate Account");
            });

            return Response::json('user created - activate account', 200);
        }

        return Response::json(['messages' => $validator->messages()->toArray()], 400);
    }

    public function activate($id, $auth)
    {
        $user = User::find($id);
        if ($user && $user->auth == $auth) {
            $user->active = 1;
            $user->save();
            return Response::json('account activated', 200);
        }
        return Response::json('error activating account', 400);
    }
}