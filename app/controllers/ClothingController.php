<?php

class ClothingController extends BaseAdController
{
    public $rules = array(
        'name'     => 'required',
        'content'   => 'required',
        'price'     => 'required',
    );
    public $relation = 'clothing';
    public $rootId = 10;
}