<?php

class FeedbackController extends BaseController {

    public function getIndex()
    {
        return View::make('feedback.index');
    }

    public function postIndex()
    {
        $rules = array(
            'first-name'    => 'required',
            'email'         => 'required|email',
            'feedback'      => 'required|min:15'
        );

        $attributes = array(
            'first-name'    => 'first name',
            'email'         => 'email',
        );

        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($attributes);

        if ($validator->fails()) {
            return Redirect::to('feedback')->withErrors($validator)->withInput();
        }

        $data = array(
            'firstName' => Input::get('first-name'),
            'lastName'  => Input::get('last-name'),
            'email'     => Input::get('email'),
            'type'      => Input::get('type'),
            'feedback'  => Input::get('feedback'),
        );

        // if the type of support is a school request or recommendation send to Kyle and Connor
        if (Input::get('type') == "request" || Input::get('type') == "recommend") {

            Mail::send('emails.feedback', $data, function($message)
            {
                $message->from('feedback@hallexchange.com', "Feedback");
                $message->to("kyle.mccarthy@hallexchange.com")
                    ->cc("connor.hall@hallexchange.com")
                    ->subject("[" . Input::get('type') . "] HallExchange Feedback");
            });

        } else {
            Mail::send('emails.feedback', $data, function($message)
            {
                $message->from('feedback@hallexchange.com', "Feedback");
                $message->to("kyle.mccarthy@hallexchange.com")
                    ->subject("[" . Input::get('type') . "] HallExchange Feedback");
            });
        }

        return Redirect::to('feedback')->with("success", "We have sent your message!");
    }
}