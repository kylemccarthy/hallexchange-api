<?php

class ExchangeController extends BaseController {

    /**
     * Return the ads for the user's university.  Include all the categories and
     * paginate the results for 15 items.
     */
    public function getIndex()
    {
        $adsTotal = Ad::where('university_id', '=', Auth::user()->getUniversityId())
            ->where('display_group_only', '=', 0)
            ->orderBy('created_at', 'desc')
            ->with('images')
            ->remember(3);

        $count = $adsTotal->count();
        $ads = $adsTotal->paginate(15);

        return View::make('exchange')
            ->with('ads', $ads)
            ->with('adCount', $count);;
    }

    /**
     * Return the ads for a specific category within a users university.  If the user
     * tries to pass an invalid category 404.  Paginate for 15 items.
     */
    public function getCategory($table, $childOne = null, $childTwo = null)
    {
        if($childOne) {
            $category = Category::find($childOne);
        }else {
            $category = Category::where('table_name', '=', $table)->first();
        }

        if ($category == null) {
            App::abort(404);
        }

        $children = $category->findDescendants()->lists('id');

        $adsTotal = Ad::where('university_id', '=', Auth::user()->getUniversityId())
            ->whereIn('category_id', $children)
            ->where('display_group_only', '=', 0)
            ->orderBy('created_at', 'desc')
            ->with('images', $category->getTableName())
            ->remember(10);

        $count = $adsTotal->count();
        $ads = $adsTotal->paginate(15);

        return View::make('exchange')
            ->with('ads', $ads)
            ->with('adCount', $count);
    }

}