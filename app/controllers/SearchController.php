<?php

class SearchController extends BaseController {

    /**
     * Handle the search query.  Search the title field of the user's university
     */
    public function postAds()
    {
        $search = Input::get('search');

        if ($search) {
            $ads = Ad::where('university_id', '=', Auth::user()->getUniversityId())->
                where('name', 'LIKE', "%" . $search . "%")->take(20)->get();
            return View::make('search')->with('ads', $ads);
        }

        return Redirect::to('/exchange');

    }

}