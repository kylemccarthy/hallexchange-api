<?php

class DashboardController extends BaseController {

    /**
     * Handle the dashboard for the default page
     */
    public function getIndex()
    {
        $ads = Ad::where('user_id', '=', Auth::user()->getUserId())
            ->orderBy('created_at', 'desc')
            ->with('images')
            ->paginate(15);
        return View::make('dashboard.index')->with('ads', $ads);
    }

    /**
     * Handle dispalying the form to edit their profile
     */
    public function getProfile()
    {
        return View::make('dashboard.editprofile')->with('user', Auth::user());
    }

    /**
     * Handle updating the users profile
     */
    public function postUpdateProfile($id)
    {
        $validator = User::updateUserValidate(Input::all());

        // validate the user and make sure they signed up with a school email
        $validatorSuccess = $validator->passes();

        if ($validatorSuccess) {
            // create the new user
            $user = User::find($id);
            $user->setFirstName(Input::get('first_name'));
            $user->setLastName(Input::get('last_name'));
            if (Input::has('pass1')) {
                $user->setPassword(Input::get('pass1'));
            }
            $user->setPhone(Input::get('phone'));
            $user->save();

            // redirect to the success page
            return Redirect::to('/dashboard/profile')->with('message', 'Your account information
            has successfully been updated.');
        } else {
            // there was an error registering the user so redirect them
            return Redirect::to('/dashboard/profile')->withErrors($validator)
                ->withInput(Input::except('pass1', 'pass2'));
        }
    }


}