<?php

class AdminController extends BaseController {

    var $admin;

    public function __construct()
    {
        if (Auth::user()->getLevel() != 10) {
            App::abort(403, 'Unauthorized action.');
        }

        $this->admin = new Admin();
    }

    public function getIndex()
    {
        $users = User::with('university')->orderBy('id', 'desc')
            ->remember(2)->paginate(50);
        $moderate = Group::where('active', '=', '0')->with('GroupCategory', 'University')
            ->get();
        return View::make('admin.index')
            ->with('users', $users)
            ->with('groups', $moderate)
            ->with('moderationQueue', $moderate->count());
    }

    public function getGroups()
    {
        $groups = Group::where('active', '=', '0')->with('GroupCategory', 'University')->get();
        return View::make('admin.groups')->
            with('groups', $groups);
    }

    public function getWeek()
    {
        return Response::json($this->admin->getNewUsersThisWeek());
    }

    public function getDay()
    {
        return Response::json($this->admin->getNumNewUsersToday());
    }

    public function getUni()
    {
        return Response::json($this->admin->getNumUsersByUniversity());
    }

    public function getDayByUni()
    {
        return Response::json($this->admin->getNewUsersTodayByUniversity());
    }

}