<?php

class TextController extends BaseController {

    public function postContactOwner($ownerId, $id)
    {
        $owner      = User::find($ownerId);
        $user       = Auth::user();
        $message    = Input::get('message');

        // make sure that string is less that 140 characters, this will help reduce cost
        // and will force them to use email for large messages
        if (strlen($message > 140)) {
            $message = substr($message, 0, 140);
        }

        $text = new TextMessage();
        $text->setReceiver($owner->getPhone());
        $text->setMessage("From " . $user->getPhone() . ": " . $message);
        $status = $text->send();

        return Redirect::to('/ads/view/' . $id)
            ->with('contactSuccess',
                "We have sent your text to the owner to let them know that you
                are interested in their ad.");
    }

}