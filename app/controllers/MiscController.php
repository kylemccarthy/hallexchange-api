<?php

class MiscController extends BaseAdController
{
    public $rules = array(
        'name'      => 'required',
        'content'   => 'required',
    );
    public $relation = 'misc';
    public $rootId = 7;
}