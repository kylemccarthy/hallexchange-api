<?php

class NewsletterController extends BaseController {

    public function getIndex()
    {
        return View::make('newsletter.index');
    }
    public function postJoin()
    {
        // validate the email for the newsletter
        $rules = array(
            'newsletter'    => 'required|email|unique:newsletter',
        );
        $attributes = array(
            'newsletter'    => 'email address',
        );
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($attributes);

        if ($validator->fails()) {
            return Redirect::to("/newsletter")
                ->withErrors($validator)
                ->withInput();
        } else {
            // info is valid.  add them to the DB and send an activation email
            $subscriber = new Newsletter();
            $subscriber->setEmail(Input::get('newsletter'));
            $subscriber->deactivate();
            $subscriber->setAuth(md5(rand()));
            $subscriber->save();

            $data = array(
                'id'    => $subscriber->getId(),
                'auth'  => $subscriber->getAuthCode(),
            );

            // send the activation email
            Mail::send('emails.subscribe', $data, function($message) use ($subscriber)
            {
                $message->from("no-reply@hallexchange.com", "HallExchange");
                $message->to($subscriber->getEmail())->subject("HallExchange Newsletter");
            });

            return Redirect::to("/newsletter")
                ->with('success', "We have added you to our newsletter!")
                ->withInput();
        }
    }

    public function getActivate($user, $auth)
    {
        $subscriber = Newsletter::find($user);
        if ($subscriber) {
            if ($subscriber->getAuthCode() == $auth) {
                $subscriber->activate();
                $subscriber->save();
                return View::make('newsletter.activate')->with('status', 'success');
            }
        } else {
            return View::make('newsletter.activate')->with('status', 'error');
        }
    }

    public function getDeactivate($user, $auth)
    {
        $subscriber = Newsletter::find($user);
        if ($subscriber) {
            if ($subscriber->getAuthCode() == $auth) {
                $subscriber->delete();
                return View::make('newsletter.deactivate')->with('status', 'success');
            }
        } else {
            return View::make('newsletter.deactivate')->with('status', 'error');
        }
    }

    public function getSendPromo()
    {
        $items = Newsletter::where('sent', '=', 0)->where('active', '=', 1)->take(5000)->get();

        foreach ($items as $item) {
            $data = array(
                'id' => $item->getId(),
                'code' => $item->getAuthCode(),
            );

            echo $item->getEmail() . ' ' . $item->getAuthCode() . '<br>';

            // send promo email
            Mail::send('emails.endpromo', $data, function($message) use ($item)
            {
                $message->from("no-reply@hallexchange.com", "HallExchange");
                $message->to($item->getEmail())->subject("Done with your books? Perfect.");
            });

            $item->setSent(1);
            $item->save();
        }

    }

    public function getRemindActivate()
    {
        $users = User::where('active', '=', '0')->get();

        foreach ($users as $user) {
            // set the password
            $password = User::randomPassword();
            $user->setPassword($password);
            $user->save();

            // build data for email
            $data = array(
                'id' => $user->getUserId(),
                'name' => $user->getFirstName(),
                'token' => $user->getAuthToken(),
                'pass' => $password,
            );

            // send email

            Mail::send('emails.activate-remind', $data, function($message) use ($user)
            {
                $message->from("no-reply@hallexchange.com", "HallExchange");
                $message->to($user->getEmail())->subject("[HallExchange] Activate Account Reminder");
            });

            echo $user->getEmail() . '<br>';
        }
    }
}