<?php namespace App\Lib\CdnImage\Facades;

use Illuminate\Support\Facades\Facade;

class CdnImage extends Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'cdnimage'; }
}