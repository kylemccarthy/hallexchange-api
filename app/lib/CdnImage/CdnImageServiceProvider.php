<?php namespace App\Lib\CdnImage;

use Illuminate\Support\ServiceProvider;

class CdnImageServiceProvider extends ServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app['cdnimage'] = $this->app->share(function($app)
        {
            return new CdnImage;
        });

        $this->app->booting(function()
        {
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();
            $loader->alias('CdnImage', 'App\Lib\CdnImage\Facades\CdnImage');
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

}
