<?php namespace App\Lib\Apiauth;

use Illuminate\Support\ServiceProvider;

class ApiauthServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
        $this->app['apiauth'] = $this->app->share(function($app)
        {
            return new Apiauth;
        });

        $this->app->booting(function()
        {
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();
            $loader->alias('Apiauth', 'App\Lib\Apiauth\Facades\Apiauth');
        });
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}
