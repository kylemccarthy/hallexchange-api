<?php namespace App\Lib\Apiauth;

class Apiauth {

    public function createClient()
    {
        $client = new Models\Apiclient();
        $client->key = $client->generateKey();
        $client->save();
        return $client;
    }

    public function createUser(array $input)
    {
        $model = Models\Apiuser::create($input);
        $model->key = $model->generateKey();
        $model->save();
        return $model;
    }

    public function authenticate(array $input, $header)
    {
        $user = \Auth::attempt($input);
        if ($user) {
            $user = \Auth::user();
            $client = Models\Apiclient::where('key', '=', $header)->first();
            if ($client) {
                $key = $this->createUser(array(
                    'user_id' => $user->id,
                ));
                if (!$user->active) {
                    return \Response::json('account inactive', 403);
                }
                return \Response::json(array(
                    $user->toArray(),
                    'key' => $key->toArray(),
                ), 200);
            } else {
                return \Response::json('bad client', 400);
            }
        } else {
            return \Response::json('bad login', 400);
        }
    }

    /**
     * Return a false or the user id in an attempt to validate the client_id header and user_id
     * header passed to the server.
     *
     * @param $client
     * @param $user
     * @return bool
     */
    public function restricted($client, $user)
    {
        $client = Models\Apiclient::where('key', '=', $client)->first();
        $user = Models\Apiuser::where('key', '=', $user)->first();
        if (!count($client) > 0) {
            return false;
        }
        if (!count($user) > 0) {
            return false;
        }
        return $user->user_id;
    }
}