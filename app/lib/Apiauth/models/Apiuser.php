<?php namespace App\Lib\Apiauth\Models;

class Apiuser extends Api {

    protected $table = 'api_sessions';
    public $timestamps = false;
    protected $softDelete = false;
    protected $fillable = array('key', 'user_id');

}