<?php namespace App\Lib\Apiauth\Models;

abstract class Api extends \Eloquent {

    public function generateKey()
    {
        return \Str::random(40);
    }
}