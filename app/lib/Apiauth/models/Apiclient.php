<?php namespace App\Lib\Apiauth\Models;

class Apiclient extends Api {

    protected $table = 'api_clients';
    public $timestamps = false;
    protected $softDelete = false;
    protected $fillable = array('key');

}