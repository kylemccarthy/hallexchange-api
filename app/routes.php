<?php

Route::group(array('prefix' => 'v1'), function()
{
    // login routes
    Route::post('login', array('uses' => 'UserController@login'));

    // register routes
    Route::post('register', array('uses' => 'UserController@store'));

    // un-protected user routes
    Route::get('user/{id}/activate/{auth}', array('uses' => 'UserController@activate'));

    // home routes
    Route::get('home/recent', array('uses' => 'HomeController@recentAds'));
    Route::get('home/universities', array('uses' => 'HomeController@universities'));

    Route::group(array('before' => 'auth.restricted'), function()
    {
        // ad routes
        Route::get('ad', array('uses' => 'AdController@index'));
        Route::get('ad/group', array('uses' => 'AdController@universityGroup'));
        Route::get('ad/{id}', array('uses' => 'AdController@show'));
        Route::get('ad/{id}/delete', array('uses' => 'AdController@delete'));

        // book routes
        Route::get('book', array('uses' => 'BookController@index'));
        Route::get('book/group', array('uses' => 'BookController@universityGroup'));
        Route::post('book', array('uses' => 'BookController@store'));
        Route::post('book/{id}', array('uses' => 'BookController@update'));

        // clothing routes
        Route::get('clothing', array('uses' => 'ClothingController@index'));
        Route::get('clothing/group', array('uses' => 'ClothingController@universityGroup'));
        Route::post('clothing', array('uses' => 'ClothingController@store'));
        Route::post('clothing/{id}', array('uses' => 'ClothingController@update'));
        
        // electronic routes
        Route::get('electronic', array('uses' => 'ElectronicController@index'));
        Route::get('electronic/group', array('uses' => 'ElectronicController@universityGroup'));
        Route::post('electronic', array('uses' => 'ElectronicController@store'));
        Route::post('electronic/{id}', array('uses' => 'ElectronicController@update'));
        
        // furniture routes
        Route::get('furniture', array('uses' => 'FurnitureController@index'));
        Route::get('furniture/group', array('uses' => 'FurnitureController@universityGroup'));
        Route::post('furniture', array('uses' => 'FurnitureController@store'));
        Route::post('furniture/{id}', array('uses' => 'FurnitureController@update'));
        
        // housing routes
        Route::get('housing', array('uses' => 'HousingController@index'));
        Route::get('housing/group', array('uses' => 'HousingController@universityGroup'));
        Route::post('housing', array('uses' => 'HousingController@store'));
        Route::post('housing/{id}', array('uses' => 'HousingController@update'));
        
        // misc routes
        Route::get('misc', array('uses' => 'MiscController@index'));
        Route::get('misc/group', array('uses' => 'MiscController@universityGroup'));
        Route::post('misc', array('uses' => 'MiscController@store'));
        Route::post('misc/{id}', array('uses' => 'MiscController@update'));
        
        // promotion routes
        Route::get('promotion', array('uses' => 'PromotionController@index'));
        Route::get('promotion/group', array('uses' => 'PromotionController@universityGroup'));
        Route::post('promotion', array('uses' => 'PromotionController@store'));
        Route::post('promotion/{id}', array('uses' => 'PromotionController@update'));
        
        // ride routes
        Route::get('ride', array('uses' => 'RideController@index'));
        Route::get('ride/group', array('uses' => 'RideController@universityGroup'));
        Route::post('ride', array('uses' => 'RideController@store'));
        Route::post('ride/{id}', array('uses' => 'RideController@update'));
        
        // ticket routes
        Route::get('ticket', array('uses' => 'TicketController@index'));
        Route::get('ticket/group', array('uses' => 'TicketController@universityGroup'));
        Route::post('ticket', array('uses' => 'TicketController@store'));
        Route::post('ticket/{id}', array('uses' => 'TicketController@update'));
        
        // image routes -- NEED TO WORK ON THIS!!
        Route::post('image/upload/{id}', array('uses' => 'ImageController@store'));
        Route::post('image/{id}/delete', array('uses' => 'ImageController@delete'));

        // user routes
        Route::get('user', array('uses' => 'UserController@index'));
        Route::post('user', array('uses' => 'UserController@update'));
        Route::get('user/posts', array('uses' => 'UserController@posts'));

        // messaging
        Route::get('message', array('uses' => 'MessageController@index'));
        Route::post('message/new/{id}', array('uses' => 'MessageController@newConversation'));
        Route::get('message/conversation/{id}', array('uses' => 'MessageController@getConversation'));
        Route::post('message/reply/{id}', array('uses' => 'MessageController@reply'));
    });
});