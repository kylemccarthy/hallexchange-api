<?php

class CategoryTableSeeder extends Seeder {

    public function run()
    {
        Category::mapArray( [
            [
                'name'  => 'book',
                'table' => 'books',
            ],
            [
                'name'  => 'electronic',
                'table' => 'electronics',
            ],
            [
                'name'  => 'furniture',
                'table' => 'furniture',
            ],
            [
                'name'  => 'housing',
                'table' => 'housing',
            ],
            [
                'name'  => 'ride',
                'table' => 'rides',
            ],
            [
                'name'  => 'ticket',
                'table' => 'tickets',
            ],
            [
                'name'  => 'misc',
                'table' => 'misc',
            ],
            [
                'name'  => 'promotion',
                'table' => 'promotions',
            ],
            [
                'name'  => 'tutor',
                'table' => 'tutors',
            ],
            [
                'name'  => 'clothing',
                'table' => 'clothing',
                'children'  =>
                    [
                        [
                            'name'  => 'men',
                            'table' => null,
                            'children'  =>
                                [
                                    [
                                        'name'  => 'shoes',
                                        'table' => null,
                                    ],
                                    [
                                        'name'  => 'shorts',
                                        'table' => null,
                                    ],
                                    [
                                        'name'  => 'pants',
                                        'table' => null,
                                    ],
                                    [
                                        'name'  => 'belts',
                                        'table' => null,
                                    ],
                                    [
                                        'name'  => 'shirts',
                                        'table' => null,
                                    ],
                                    [
                                        'name'  => 'sweaters',
                                        'table' => null,
                                    ],
                                    [
                                        'name'  => 'jackets',
                                        'table' => null,
                                    ],
                                    [
                                        'name'  => 'hats',
                                        'table' => null,
                                    ],
                                    [
                                        'name'  => 'sunglasses',
                                        'table' => null,
                                    ],
                                    [
                                        'name'  => 'business',
                                        'table' => null,
                                    ],
                                    [
                                        'name'  => 'misc',
                                        'table' => null,
                                    ],
                                ],
                            ],
                        [
                            'name'  => 'women',
                            'table' => null,
                            'children'  =>
                                [
                                    [
                                        'name'  => 'shoes',
                                        'table' => null,
                                    ],
                                    [
                                        'name'  => 'shorts',
                                        'table' => null,
                                    ],
                                    [
                                        'name'  => 'pants',
                                        'table' => null,
                                    ],
                                    [
                                        'name'  => 'belts',
                                        'table' => null,
                                    ],
                                    [
                                        'name'  => 'shirts',
                                        'table' => null,
                                    ],
                                    [
                                        'name'  => 'sweaters',
                                        'table' => null,
                                    ],
                                    [
                                        'name'  => 'jackets',
                                        'table' => null,
                                    ],
                                    [
                                        'name'  => 'hats',
                                        'table' => null,
                                    ],
                                    [
                                        'name'  => 'sunglasses',
                                        'table' => null,
                                    ],
                                    [
                                        'name'  => 'dresses',
                                        'table' => null,
                                    ],
                                    [
                                        'name'  => 'business',
                                        'table' => null,
                                    ],
                                    [
                                        'name'  => 'misc',
                                        'table' => null,
                                    ],
                                ],
                        ],
                    ],
            ],
        ]);
    }

}