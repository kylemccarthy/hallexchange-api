<?php

class UniversityTableSeeder extends Seeder {

    public function run()
    {
        DB::table('universities')->delete();
        University::create(array('id' => 1, 'university_name' => 'University of Missouri - Columbia'));
        University::create(array('id' => 2, 'university_name' => 'Florida State University'));
        University::create(array('id' => 3, 'university_name' => 'University of Iowa'));
        University::create(array('id' => 4, 'university_name' => 'Rockhurts University'));
    }

}