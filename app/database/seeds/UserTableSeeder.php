<?php

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();
        User::create(array(
            'id' => 1,
            'university_id' => 1,
            'first_name'    => 'Kyle',
            'last_name'     => 'McCarthy',
            'password'      => Hash::make('kyle101593'),
            'email'         => 'kjmd54@mail.missouri.edu',
            'auth'          => '573ffdadc8145786b7f9d5826baa4fad',
            'level'         => 1,
            'active'        => 1,
            'phone'         => '3144430370',
        ));
    }

}