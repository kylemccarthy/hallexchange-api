<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePremiumTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('premium', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
			$table->integer('university_id');
			$table->string('name');
			$table->string('image_url');
			$table->text('content');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('premium');
	}

}
