<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHousingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('housing', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('location')->nullable();
			$table->string('price')->nullable();
			$table->integer('ad_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('housing');
	}

}
