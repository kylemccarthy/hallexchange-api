<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('university_id');
			$table->timestamps();
			$table->string('first_name');
			$table->string('last_name')->nullable()->default('');
			$table->string('password', 60);
			$table->string('email')->unique();
			$table->string('auth');
			$table->string('phone', 11)->nullable()->default('');
			$table->integer('level')->nullable();
			$table->integer('active')->nullable();
			$table->string('remember_token')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
