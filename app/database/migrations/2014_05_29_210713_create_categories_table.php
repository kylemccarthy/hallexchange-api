<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('categories', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('path')->nullable();
			$table->integer('parent_id')->unsigned()->nullable()->index('`categories_parent_id_foreign`');
			$table->integer('level')->default(0);
			$table->timestamps();
			$table->string('name')->nullable();
			$table->string('table_name')->nullable();
			$table->index(['path','parent_id','level']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('categories');
	}

}
