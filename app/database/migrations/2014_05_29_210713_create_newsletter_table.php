<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNewsletterTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('newsletter', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('newsletter')->default('');
			$table->string('auth')->default('');
			$table->integer('active');
			$table->timestamps();
			$table->integer('sent')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('newsletter');
	}

}
