<?php namespace App\Services;

use Imagine\Image\Box;
use Imagine\Image\Point;
use File;
use Log;
use Thomaswelton\LaravelRackspaceOpencloud\Facades\OpenCloud;

class ImageService {

    protected $imagine;
    protected $quality;
    protected $image;
    protected $newFileName;

    public function __construct()
    {
        $this->imagine      = new \Imagine\Gd\Imagine();
        $this->quality      = 70;
    }

    /**
     * Handle resizing of images.  If the size is greater than 800 reduce it d
     * and the quality of the photo.
     *
     * @return string - path of image
     */
    protected function resize()
    {
        // if the photo is greater than 800px, resize and reduce the image quality
        if ($this->image->getSize()->getWidth() > 800)
        {
            // calculate the ratio of the reduction, this will allow us to keep
            // the aspects of the photo
            $width  = $this->image->getSize()->getWidth();
            $height = $this->image->getSize()->getHeight();
            $ratio  = 800/$width;

            $this->image
                ->resize(new Box(800, $height * $ratio))
                ->save('uploads/' . $this->newFileName, array(
                        'quality' => $this->quality,
                    ));
        } else {
            $this->image->save('uploads/' . $this->newFileName, array(
                        'quality' => $this->quality,
                    ));
        }

        return public_path() . '/uploads/' . $this->newFileName;
    }

    /**
     * Create a thumbnail image.
     *
     * @return string - path of image
     */
    protected function createThumb()
    {
        $this->image
            ->thumbnail(new Box(290, 120))
            ->save('uploads/' . 'thumb_' . $this->newFileName, array(
                    'quality' => $this->quality,
                ));

        return public_path() . '/uploads/thumb_' . $this->newFileName;
    }

    /**
     * Resize the photo and create a thumbnail image and upload both to the
     * CDN.
     */
    public function upload($file)
    {
        $this->image        = $this->imagine->open($file->getRealPath());
        $this->newFileName  = md5(time() . $file->getClientOriginalName() .
                rand(0, 1000)) . "." . $file->getClientOriginalExtension();

        OpenCloud::upload("hallexchange", $this->resize(), $this->newFileName);
        OpenCloud::upload("hallexchange", $this->createThumb(), "thumb_" . $this->newFileName);

        $this->clean();

        return $this->newFileName;
    }

    /**
     * remove the temporary uploads
     */
    public function clean()
    {
        File::delete(public_path() . '/uploads/' . $this->newFileName);
        File::delete(public_path() . '/uploads/thumb_' . $this->newFileName);
    }

}