<?php namespace App\Services;

class TextMessage {

    private $to, $from, $content, $apiKey, $apiSecret, $uri, $response, $cost;

    public function __construct()
    {
        $this->apiKey = "77b77740";
        $this->apiSecret = "0c0491f2";
        $this->uri = "https://rest.nexmo.com/sms/json";
        $this->from = "17142940280";
        $this->cost = 0;
    }

    /**
     * Setters: set the fields for sending a text
     */
    public function setSender($sendFrom)
    {
        $this->from = urlencode($this->formatPhoneNumbers("1" . $sendFrom));
    }

    public function setReceiver($sendTo)
    {
        $this->to = urlencode($this->formatPhoneNumbers("1" . $sendTo));
    }

    public function setMessage($content)
    {
        $this->content = urlencode(mb_convert_encoding($content, "UTF-8"));
    }

    private function setResponse($response)
    {
        $this->response = $response;
    }

    private function setCost($cost)
    {
        $this->cost = $cost;
    }

    /**
     * Getters
     */
    public function getSender() { return $this->from; }
    public function getReciver() { return $this->to; }
    public function getMessage() { return $this->content; }
    private function getKey() { return $this->apiKey; }
    private function getSecret() { return $this->apiSecret; }
    public function getResponse() { return $this->response; }
    public function getCost() { return $this->cost; }

    /**
     * Send a text message
     */
    public function send()
    {
        $post = array(
            'from'  => $this->getSender(),
            'to'    => $this->getReciver(),
            'text'  => $this->getMessage(),
            'type'  => 'text',
        );

        return $this->sendRequest($post);

    }

    /**
     * Handle the send request for a text.
     */
    private function sendRequest($postData)
    {
        $post = null;

        $data = array_merge($postData, array(
            'username'  => $this->getKey(),
            'password'  => $this->getSecret(),
        ));

        foreach ($data as $key => $value) {
            $post .= "&$key=$value";
        }

        // send CURL request to Nexmo for text
        $toNexmo = curl_init($this->uri);
        curl_setopt($toNexmo, CURLOPT_POST, true);
        curl_setopt($toNexmo, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($toNexmo, CURLOPT_POSTFIELDS, $post);
        curl_setopt($toNexmo, CURLOPT_SSL_VERIFYPEER, false);

        // Nexmo response
        $fromNexmo = curl_exec($toNexmo);
        curl_close($toNexmo);

        // convert response
        $fromNexmo = str_replace('-', '', $fromNexmo);

        return $this->parse($fromNexmo);
    }

    /**
     * parse JSON response from the server
     */
    private function parse($fromNexmo)
    {
        $response = $this->normaliseKeys(json_decode($fromNexmo));

        if ($response) {
            $this->response = $response;
        } else {
            $this->setResponse(array());
            return false;
        }
    }

    /**
     * Help to correctly format the phone numbers
     */
    private function formatPhoneNumbers($inp){
        // Remove any invalid characters
        $ret = preg_replace('/[^a-zA-Z0-9]/', '', (string)$inp);

        if(preg_match('/[a-zA-Z]/', $inp)){
            // Alphanumeric format so make sure it's < 11 chars
            $ret = substr($ret, 0, 11);
        } else {
            // Numerical, remove any prepending '00'
            if(substr($ret, 0, 2) == '00'){
                $ret = substr($ret, 2);
                $ret = substr($ret, 0, 15);
            }
        }

        return (string)$ret;
    }

    /**
     * Recursively normalise any key names in an object, removing unwanted characters
     */
    private function normaliseKeys ($obj) {
        // Determine is working with a class or araay
        if ($obj instanceof stdClass) {
            $new_obj = new stdClass();
            $is_obj = true;
        } else {
            $new_obj = array();
            $is_obj = false;
        }

        foreach($obj as $key => $val){
            // If we come across another class/array, normalise it
            if ($val instanceof stdClass || is_array($val)) {
                $val = $this->normaliseKeys($val);
            }

            // Replace any unwanted characters in they key name
            if ($is_obj) {
                $new_obj->{str_replace('-', '', $key)} = $val;
            } else {
                $new_obj[str_replace('-', '', $key)] = $val;
            }
        }

        return $new_obj;
    }

}