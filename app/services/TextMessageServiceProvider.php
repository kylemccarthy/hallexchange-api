<?php namespace App\Services;

use Illuminate\Support\ServiceProvider;
use App;

class TextMessageServiceProvider extends ServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app['textmessage'] = $this->app->share(function($app)
        {
            return new App\Services\TextMessage;
        });
    }
}
