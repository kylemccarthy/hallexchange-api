<?php namespace App\Services;

use Illuminate\Support\ServiceProvider;
use App;

class ImageServiceProvider extends ServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app['imageservice'] = $this->app->share(function($app)
        {
            return new App\Services\ImageService;
        });
    }
}
