<?php

class BookTest extends Illuminate\Foundation\Testing\TestCase {

    /**
     * Creates the application.
     *
     * @return Symfony\Component\HttpKernel\HttpKernelInterface
     */
    public function createApplication()
    {
        $unitTesting = true;

        $testEnvironment = 'testing';

        return require __DIR__.'/../../bootstrap/start.php';
    }

    public function testCreate()
    {
        $postData = array(
            'user_id'       => 1,
            'university_id' => 1,
            'category_id'   => 1,
            'group_id'      => null,
            'name'          => 'Test Case',
            'display_group_only' => null,
            'content'       => 'Just testing the book creation',
            'isbn'          => '1234567891234',
            'course'        => 'CS 2270',
            'price'         => '25'
        );
        $this->call('POST', 'book', $postData);
        $this->assertResponseOk();
    }

    public function testUpdate()
    {
        $postData = array(
            'group_id'      => null,
            'name'          => 'Test Case update',
            'display_group_only' => null,
            'content'       => 'Just testing the book update',
            'isbn'          => '9876554321',
            'course'        => 'CS 3330',
            'price'         => '30'
        );
        $this->call('PUT', 'book/327', $postData);
        $this->assertResponseOk();
    }

    /*public function testDelete()
    {
        $this->call('DELETE', 'book/328');
        $this->assertResponseOk();
    }

    public function testShow()
    {
        $this->call('GET', 'book/329');
        $this->assertResponseOk();
    }*/
}