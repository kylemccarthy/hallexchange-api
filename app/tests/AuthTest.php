<?php

class AuthTest extends Illuminate\Foundation\Testing\TestCase {

    /**
     * Creates the application.
     *
     * @return Symfony\Component\HttpKernel\HttpKernelInterface
     */
    public function createApplication()
    {
        $unitTesting = true;

        $testEnvironment = 'testing';

        return require __DIR__.'/../../bootstrap/start.php';
    }

    /*public function testLogin()
    {
        $post = array(
            'email' => 'kyle.mccarthy@hallexchange.com',
            'password' => 'testtest'
        );
        $this->call('POST', '/login', $post, array(), array('HTTP_CLIENT_ID' => '$2y$10$azHBDZHJCs6cVN3O/QXp2eK/lXjf6mlUqllx/Y660pUzYVZ3XeIvO'));
    }*/

    public function testAuth()
    {
        $post = array(
            'email' => 'kyle.mccarthy@hallexchange.com',
            'password' => 'testtest'
        );
        $api = new \Kylemccarthy\Apiauth\Apiauth();
        $api->authenticate($post, '$2y$10$azHBDZHJCs6cVN3O/QXp2eK/lXjf6mlUqllx/Y660pUzYVZ3XeIvO');
    }
}