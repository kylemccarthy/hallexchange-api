<?php

class Housing extends BaseAd {

    protected $table = 'housing';
    public $timestamps = false;
    protected $softDelete = false;
    protected $fillable = array('location', 'price', 'ad_id');

    public static function create(array $input)
    {
        $relation = 'housing';
        parent::make($input, $relation);
    }
}