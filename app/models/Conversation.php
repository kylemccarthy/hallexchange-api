<?php

class Conversation extends Eloquent {

    protected $table = 'conversations';
    public $timestamps = true;
    protected $softDelete = false;
    protected $fillable = array('owner', 'buyer', 'ad_id');

    public function messages()
    {
        return $this->hasMany('Message');
    }

    public function viewable()
    {

    }

}