<?php

class Image extends Eloquent {

	protected $table = 'images';
	public $timestamps = true;
	protected $softDelete = false;
    protected $baseUrl = "https://9b8a8110712cb03fecea-9c1a5d2675a7aad509aeaa3fcf36a590.ssl.cf2.rackcdn.com/";
    protected $fillable = array('user_id', 'ad_id', 'path');

	public function ad()
	{
		return $this->belongsTo('Ad', 'ad_id');
	}

	public function user()
	{
		return $this->belongsTo('User', 'user_id');
	}

    public static function upload(array $input)
    {
        foreach ($input[1] as $file)
        {
            $path = CdnImage::upload($file);
            Image::create(array(
                'user_id'   => Auth::user()->id,
                'ad_id'     => $input[0]->id,
                'path'      => $path,
            ));
        }
    }

    /**
     * Remove the image from the CDN and from the DB.
     */
    public function delete()
    {
        OpenCloud::delete("hallexchange", $this->getName());
        OpenCloud::delete("hallexchange", "thumb_" . $this->getName());
        parent::delete();
    }

}