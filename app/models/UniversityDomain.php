<?php

class UniversityDomain extends Eloquent {

    protected $table = 'university_domains';
    public $timestamps = false;
    protected $softDelete = false;

    public function university()
    {
        return $this->belongsTo('University');
    }
}