<?php

class Group extends Eloquent {

	protected $table = 'groups';
	public $timestamps = true;
	protected $softDelete = false;

    /**
     * Validate the group
     */
    public static function groupValidate($input)
    {
        $rules = array(
            'name'    => 'required',
            'description'     => 'required',
        );

        $validator = Validator::make($input, $rules);

        return $validator;
    }

}