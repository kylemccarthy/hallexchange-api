<?php

abstract class BaseAd extends Eloquent {

    public $timestamps = false;
    protected $softDelete = false;

    public function ad()
    {
        return $this->belongsTo('Ad');
    }

    public static function make(array $input, $relation)
    {
        $model = new static($input);
        $ad = Ad::create($input);
        $ad->save();
        $ad->$relation()->save($model);
        return $ad;
    }
}