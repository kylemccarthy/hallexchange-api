<?php

class GroupItem extends Eloquent {

	protected $table = 'group_members';
	public $timestamps = false;
	protected $softDelete = false;

	public function user()
	{
		return $this->belongsTo('User', 'user_id');
	}

	public function group()
	{
		return $this->belongsTo('Group', 'group_id');
	}

}