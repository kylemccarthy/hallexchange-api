<?php

class Promotion extends BaseAd {

    protected $table = 'promotions';
    public $timestamps = false;
    protected $softDelete = false;
    protected $fillable = array('ad_id');

    public static function create(array $input)
    {
        $relation = 'promotion';
        parent::make($input, $relation);
    }
}