<?php

class Clothing extends BaseAd {

    protected $table = 'clothing';
    public $timestamps = false;
    protected $softDelete = false;
    protected $fillable = array('size', 'price', 'ad_id');

    public static function create(array $input)
    {
        $relation = 'clothing';
        parent::make($input, $relation);
    }
}