<?php

class GroupCategory extends Eloquent {

    protected $table = 'group_categories';
    public $timestamps = false;
    protected $softDelete = false;

}