<?php

class Book extends BaseAd {

    protected $table = 'books';
    public $timestamps = false;
    protected $softDelete = false;
    protected $fillable = array('isbn', 'course', 'price', 'ad_id');

    public static function create(array $input)
    {
        $relation = 'book';
        $model = parent::make($input, $relation);
        return $model;
    }
}