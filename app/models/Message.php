<?php

class Message extends Eloquent {

    protected $table = 'messages';
    public $timestamps = true;
    protected $softDelete = false;
    protected $fillable = array('user_id', 'content', 'conversation_id');

    public function user()
    {
        return $this->hasOne('User');
    }

    public function conversation()
    {
        return $this->belongsTo('Conversation');
    }
}