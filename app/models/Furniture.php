<?php

class Furniture extends BaseAd {

	protected $table = 'furniture';
	public $timestamps = false;
	protected $softDelete = false;

    public static function create(array $input)
    {
        $relation = 'furniture';
        parent::make($input, $relation);
    }

    /**
     * Validate furniture
     */
    public static function validateFurniture($input)
    {
        $rules = array(
            'name'     => 'required',
            'content'   => 'required',
            'price'     => 'required',
        );

        return Validator::make($input, $rules);
    }

}