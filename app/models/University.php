<?php

class University extends Eloquent {

	protected $table = 'universities';
	public $timestamps = false;
	protected $softDelete = false;
    protected $fillable = array('name', 'university_group_id');


    public function group()
    {
        return $this->belongsTo('UniversityGroup');
    }

    public function domains()
    {
        return $this->hasMany('UniversityDomain');
    }
}