<?php

class Ride extends BaseAd {

    protected $table = 'rides';
    public $timestamps = false;
    protected $softDelete = false;
    protected $fillable = array('going_to', 'coming_from', 'type', 'date', 'ad_id');

    public static function create(array $input)
    {
        $relation = 'ride';
        parent::make($input, $relation);
    }
}