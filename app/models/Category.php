<?php

class Category extends \Gzero\EloquentTree\Model\Tree {

	protected $table = 'categories';
	public $timestamps = false;
	protected $softDelete = false;
    protected $fillable = array('name', 'table');

    public function ads()
    {
        return $this->hasMany('Ad', 'category_id');
    }

}