<?php

class Ad extends Eloquent {

    protected $table = 'ads';
    public $timestamps = true;
    protected $softDelete = false;
    protected $fillable = array('user_id', 'university_id', 'group_id', 'display_group_only', 'name', 'content');

    /**
     * Eloquent relations
     */
    public function category()
    {
        return $this->belongsTo('Category');
    }

    public function university()
    {
        return $this->belongsTo('University');
    }

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function images()
    {
        return $this->hasMany('Image');
    }

    /**
     * Ad HAS_MANY relations
     */
    public function book()
    {
        return $this->hasOne('Book');
    }

    public function clothing()
    {
        return $this->hasOne('Clothing');
    }

    public function electronic()
    {
        return $this->hasOne('Electronic');
    }

    public function furniture()
    {
        return $this->hasOne('Furniture');
    }

    public function housing()
    {
        return $this->hasOne('Housing');
    }

    public function misc()
    {
        return $this->hasOne('Misc');
    }

    public function promotion()
    {
        return $this->hasOne('Promotion');
    }

    public function ride()
    {
        return $this->hasOne('Ride');
    }

    public function ticket()
    {
        return $this->hasOne('Ticket');
    }
}