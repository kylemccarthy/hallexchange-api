<?php

class Ticket extends BaseAd {

    protected $table = 'tickets';
    public $timestamps = false;
    protected $softDelete = false;
    protected $fillable = array('date', 'price', 'ad_id');

    public static function create(array $input)
    {
        $relation = 'ticket';
        parent::make($input, $relation);
    }
}