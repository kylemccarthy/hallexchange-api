<?php

class Electronic extends BaseAd {

    protected $table = 'electronics';
    public $timestamps = false;
    protected $softDelete = false;
    protected $fillable = array('price', 'ad_id');

    public static function create(array $input)
    {
        $relation = 'electronic';
        parent::make($input, $relation);
    }
}