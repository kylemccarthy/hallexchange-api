<?php

class Newsletter extends Eloquent {

    protected $table = 'newsletter';
    public $timestamps = true;
    protected $softDelete = false;

}