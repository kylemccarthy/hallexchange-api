<?php

class UniversityGroup extends Eloquent {

    protected $table = 'university_groups';
    public $timestamps = false;
    protected $softDelete = false;
    protected $fillable = array('name');


    public function universities()
    {
        return $this->hasMany('University');
    }
}