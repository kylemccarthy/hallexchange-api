<?php

class Admin {

    /**
     * Get the number of users that have registered today.
     * @return mixed
     */
    public function getNumNewUsersToday()
    {
        return $this->newUsersToday()->count();
    }

    /**
     * Make an array of the schools and the number of users that
     * attend each school.
     * @return array
     */
    public function getNumUsersByUniversity()
    {
        $numUsers = array();
        $unis = University::remember(10)->get();
        $count = 0;

        foreach ($unis as $uni) {
            $uniObj = new App\Services\Admin\UniversityObj();
            $uniObj->name = $uni->getName();
            $uniObj->count = $this->getSchoolUsers($uni->getId())->count();
            $numUsers[$count] = $uniObj;
            $count++;
        }

        return $numUsers;
    }

    /**
     * Return an array of the number of users for the past 7 days
     *
     * @return array
     */
    public function getNewUsersThisWeek()
    {
        $numUsers = array();

        for ($i = 0; $i < 8; $i++) {
            $date = new DateTime();
            $date->add(DateInterval::createFromDateString('-' . $i .'day'));
            $dateObj = new App\Services\Admin\DateObj();
            $dateObj->day = $date->format('D');
            $dateObj->count = $this->getNumUsersByDateFormatted($date->format('Y-m-d'));
            $numUsers[$i] = $dateObj;
        }

        return $numUsers;
    }

    public function getNewUsersTodayByUniversity()
    {
        $usersByUniversity = array();

        // todays date
        $today = getdate();
        $year = $today['year'];
        $month = sprintf("%02s", $today['mon']);
        $day = sprintf("%02s", $today['mday']);
        $date = $year . "-" . $month . "-" . $day;

        // set the array uni key to the number of users from today
        $count = 0;
        foreach (University::remember(5)->get() as $university) {
            // create the obj for json
            $uniObj = new App\Services\Admin\UniversityObj();
            $uniObj->name   = $university->getName();
            $uniObj->id     = $university->getId();
            $uniObj->count  = User::where('created_at', 'LIKE', '%' . $date  . '%')
                ->where('university_id', '=', $university->getId())
                ->remember(5)->get()->count();
            // set the obj in the array
            $usersByUniversity[$count] = $uniObj;
            // inc counter
            $count++;
        }

        return $usersByUniversity;
    }

    /**
     * Get all the users that were registered on the date passed
     * @param $year
     * @param $month
     * @param $day
     * @return mixed
     */
    private function getUsersByDate($year, $month, $day)
    {
        $date = $year . '-' . $month . '-' . $day;

        return User::where('created_at', 'LIKE', '%' . $date  . '%')
            ->remember(5)->get();
    }

    private function getUsersByDateFormatted($date)
    {
        return User::where('created_at', 'LIKE', '%' . $date  . '%')
            ->remember(5)->get();
    }

    /**
     * Get all the users that have registered today.
     * @return mixed
     */
    private function newUsersToday()
    {
        $today = getdate();
        $year = $today['year'];
        $month = sprintf("%02s", $today['mon']);
        $day = sprintf("%02s", $today['mday']);

        return $this->getUsersByDate($year, $month, $day);

    }

    /**
     * Get the users based on the university they attend
     * @param $universtiyId
     * @return mixed
     */
    private function getSchoolUsers($universtiyId)
    {
        return User::where('university_id', '=', $universtiyId)
            ->remember(5)->get();
    }

    /**
     * Get the number of users created on a certain date
     * @param $year
     * @param $month
     * @param $day
     * @return mixed
     */
    private function getNumUsersByDate($year, $month, $day)
    {
        return $this->getUsersByDate($year, $month, $day)->count();
    }

    private function getNumUsersByDateFormatted($date)
    {
        return $this->getUsersByDateFormatted($date)->count();
    }
}