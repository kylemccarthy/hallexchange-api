<?php

class GroupRequest extends Eloquent {

    protected $table = 'group_requests';
    public $timestamps = false;
    protected $softDelete = false;

    public function user()
    {
        return $this->belongsTo('User');
    }

}