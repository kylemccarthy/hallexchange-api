<?php

class Misc extends BaseAd {

    protected $table = 'misc';
    public $timestamps = false;
    protected $softDelete = false;
    protected $fillable = array('price', 'ad_id');

    public static function create(array $input)
    {
        $relation = 'misc';
        parent::make($input, $relation);
    }
}