<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

    protected $table = 'users';
    protected $hidden = array('password');
    public $timestamps = true;
    protected $softDelete = false;
    protected $fillable = array('first_name', 'last_name', 'password', 'phone');

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }

    // Added for Laravel 4.1.26 upgrade
    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    /**
     * Relations
     */
    public function ads()
    {
        return $this->hasMany('Ad');
    }

    public function university()
    {
        return $this->belongsTo('University');
    }

    public function groups()
    {
        return $this->hasMany('GroupItem');
    }

}