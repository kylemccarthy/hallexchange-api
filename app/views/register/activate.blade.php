<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h3>Activate your account</h3>

<div>
    Thanks for joining the HallExchange community.  To begin buying and selling items
    you will have to activate your account.  Once you activate it you can then login
    using the password you created during the sign up process.<br><br>

    To activate your account visit
    <a href="http://api.hallexchange.dev/v1/user/{{ $id }}/activate/{{ $auth }}">
        api.hallexchange.dev/v1/user/{{ $id }}/activate/{{ $auth }}</a> <br><br>

    - HallExchange
</div>
</body>
</html>