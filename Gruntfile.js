module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            css: {
                src: [
                    'public/assets/css/bootstrap.css',
                    'public/assets/css/main.css'
                ],
                dest: 'public/assets/css/main.combined.css'
            }
        },
        cssmin : {
            css:{
                src: 'public/assets/css/main.combined.css',
                dest: 'public/assets/css/main.min.css'
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.registerTask('default', [ 'concat:css', 'cssmin:css' ]);
};
